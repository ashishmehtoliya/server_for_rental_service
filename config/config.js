require('dotenv').config();
const mongoose = require('mongoose');

console.log(process.env.MONGODB_URI);

const PORT = process.env.PORT || 3000;
const MONGODB_URI = process.env.MONGODB_URI.toString();

mongoose.connect(MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
}).then((res) => {
  console.log(`Connected to the database`);
}).catch((err) => {
  console.log(`ERROR => ${err.message}`);
});

mongoose.set('useFindAndModify', false);

module.exports = {
  PORT,
};
