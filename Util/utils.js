/* eslint-disable require-jsdoc */

const jwt = require('jsonwebtoken');

function buildQuery(q) {
  const res = {
    ...q,
  };
  res.issueDate = q.issueDate ? new Date(q.issueDate) : new Date('1960-01-01');
  res.returnDate = q.returnDate ?
    new Date(q.returnDate) : new Date('1960-01-01');
  res.capacity = parseInt(res.capacity);
  return res;
}

function buildMatchQuery(model, capacity) {
  if (model === undefined) {
    return {
      capacity: {
        $gte: capacity === undefined ? 0 : capacity,
      },
    };
  }
  const res = {
    model: model,
    capacity: {
      $gte: capacity === undefined ? 0 : parseInt(capacity),
    },
  };

  return res;
}

function authorize(req) {
  const t = getToken(req);
  const decodedToken = jwt.verify(t, process.env.SECRET);

  if (t === null || !decodedToken.id) {
    return null;
  }
  return decodedToken;
}

function getToken(req) {
  const auth = req.get('authorization');
  if (auth && auth.toLowerCase().startsWith('bearer ')) {
    return auth.substring(7);
  }
  return null;
}

module.exports = {
  buildQuery, buildMatchQuery, authorize, getToken,
};
