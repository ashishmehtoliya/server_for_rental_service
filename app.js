const express = require('express');
const carRouter = require('./controllers/car');
const bookingRouter = require('./controllers/booking');
const userRouter = require('./controllers/user');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(express.json());

app.use('/api/cars/', carRouter.router);
app.use('/api/bookings/', bookingRouter.router);
app.use('/api/user/', userRouter.router);

app.use((err, req, res, next) => {
  if (err.origin === 'JWT') {
    return res.status(401).send(err.err.message);
  }
  res.status(500).json({error: err.message});
});

module.exports = {
  app,
};
