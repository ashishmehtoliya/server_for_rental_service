const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  phoneNumber: {
    type: String,
    match: /[0-9]{5}/,
    unique: true,
    required: true,
  },

  name: {
    type: String,
    required: true,
  },
  passwordHash: {
    type: String,
    required: true,
    match: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/,
  },
  refreshToken: {
    type: String,
    minlength: 30,
    maxlength: 30,
  },
});

userSchema.set('toJSON', {
  transform: (doc, returned) => {
    returned.id = returned._id.toString();
    delete returned._id;
    delete returned.__v;
    delete returned.passwordHash;
  },
});

userSchema.post('save', (err, doc, next) => {
  if (err.code === 11000) {
    next(new Error('Number must be unique'));
  } else {
    next(err);
  }
});

module.exports = mongoose.model('User', userSchema);
