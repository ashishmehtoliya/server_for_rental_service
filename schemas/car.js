const mongoose = require('mongoose');

const requiredNumber = {
  type: Number,
  required: true,
};

const carSchema = new mongoose.Schema({
  number: {
    type: String,
    unique: true,
    required: true,
  },

  model: {
    type: String,
    required: true,
  },

  capacity: requiredNumber,
  rent: requiredNumber,
});

carSchema.post('save', (err, doc, next) => {
  if (err.code === 11000) {
    next(new Error('number must be unique'));
  } else {
    next(err);
  }
});

carSchema.set('toJSON', {
  transform: (document, returnedObj) => {
    returnedObj.id = returnedObj._id.toString();
    delete returnedObj._id;
    delete returnedObj.__v;
  },
});

module.exports = mongoose.model('Car', carSchema);
